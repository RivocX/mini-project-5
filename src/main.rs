// Import necessary libraries and modules
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use aws_config::load_from_env;
use aws_sdk_dynamodb::{Client, model::AttributeValue};
use std::collections::HashMap;
use rand::prelude::*;

// Define a struct to represent the request payload received by the Lambda function
#[derive(Deserialize)]
struct Request {
    university: Option<String>,
    course_name: Option<String>,
}

// Define a struct to represent the response payload to be returned by the Lambda function
#[derive(Serialize)]
struct Response {
    professor_name: String,
    course_id: Option<u64>,
}

// The main entry point for the Lambda function, utilizing the `tokio` runtime
#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

// The handler function responsible for processing Lambda events
async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    let (professor_name, course_id) = search_professor(&client, request.university, request.course_name).await?;

    Ok(json!({
        "professor_name": professor_name,
        "course_id": course_id,
    }))
}

// Function to search for professor information in DynamoDB based on provided parameters
async fn search_professor(client: &Client, university: Option<String>, course_name: Option<String>) -> Result<(String, Option<u64>), LambdaError> {
    let table_name = "ProfessorInfo";
    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();

    if let Some(university_val) = university {
        expr_attr_values.insert(":university_val".to_string(), AttributeValue::S(university_val));
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        filter_expression.push_str("university = :university_val");
    }

    if let Some(course_name_val) = course_name {
        if !filter_expression.is_empty() {
            filter_expression.push_str(" and ");
        }
        expr_attr_values.insert(":course_name_val".to_string(), AttributeValue::S(course_name_val));
        filter_expression.push_str("course_name = :course_name_val");
    }

    let result = client.scan()
        .table_name(table_name)
        .set_expression_attribute_values(Some(expr_attr_values))
        .set_filter_expression(Some(filter_expression))
        .send()
        .await?;

    let items = result.items.unwrap_or_default();
    let selected_item = items.iter().choose(&mut thread_rng());

    match selected_item {
        Some(item) => {
            let professor_name_attr = item.get("professor_name").and_then(|val| val.as_s().ok());
            let course_id_attr = item.get("course_id").and_then(|val| val.as_n().ok().map(|id| id.parse().ok()).flatten());

            match (professor_name_attr, course_id_attr) {
                (Some(name), Some(id)) => Ok((name.to_string(), Some(id))),
                _ => Err(LambdaError::from("No professor_name or course_id found in the selected item")),
            }
        },
        None => Err(LambdaError::from("No matching professor information found")),
    }
}
